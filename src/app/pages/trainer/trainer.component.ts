import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { TrainerService } from '../../services/trainer.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  public pokemonList: Pokemon[] = [];
  public trainer = null;


  constructor(private trainerService: TrainerService,
              private router: Router) { }

  get collectionList$(): Observable<Pokemon[]> {
    return this.trainerService.pokemonCollection;
  }

  ngOnInit(): void {
    this.trainer = localStorage.getItem("trainer");
  }

  goToDetails(id){
    this.router.navigate(['/catalogue', id]);
  }
 
  onLogoutClick(){
    localStorage.clear();
  }
}
