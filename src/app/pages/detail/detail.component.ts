import { Component, OnInit } from '@angular/core';
import { PokemonDetailService } from '../../services/pokemon-detail.service';
import { Subscription } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { ActivatedRoute } from '@angular/router';
import { TrainerService } from '../../services/trainer.service'
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  private pokemon$: Subscription;
  public pokemon: Pokemon = null;

  constructor(private pokemonDetailService: PokemonDetailService,
              private trainerService: TrainerService,
              private route: ActivatedRoute) { 
    this.pokemon$ = this.pokemonDetailService.pokemon$.subscribe((pokemon: Pokemon) => {
      this.pokemon = pokemon;
    });
  }

  onCollectClick(){
    this.trainerService.addPokemon(this.pokemon);
  }

  ngOnInit(): void {
    this.pokemonDetailService.getPokemonById(this.route.snapshot.params.id);
  }
  ngOnDestroy(): void {
    this.pokemon$.unsubscribe();
  }
}
