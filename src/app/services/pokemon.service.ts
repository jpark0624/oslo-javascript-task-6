import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  public pokemon$: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) {
  }

   public getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }


  getPokemon(): void {
    const requestUrl = environment.apiBaseUrl + "/pokemon?limit=151";
    this.http.get<Pokemon[]>(requestUrl)
      .pipe(
        map((response: any) => response.results.map((pokemon: any) => {
            return {...pokemon, ...this.getIdAndImage(pokemon.url)};
          }))).toPromise().then((p: Pokemon[]) => {
      this.pokemon$.next(p);
    } );
  }
}