import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TrainerService } from '../services/trainer.service';

@Injectable( {
  providedIn: 'root'
} )
export class AuthService implements CanActivate {

  constructor(private trainerService: TrainerService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem("trainer")) return true;
  else {
      this.router.navigateByUrl( '/login' );
      return false;
    }
  }

}
