import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  public pokemonCollection: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);

  constructor() { }

  public addPokemon(pokemon: Pokemon): void {
    const currentCollection = [...this.pokemonCollection.value];
    currentCollection.push(pokemon);
    this.pokemonCollection.next(currentCollection);
    localStorage.setItem("pokemon-collection", JSON.stringify(this.pokemonCollection.value));
   
  }
}
